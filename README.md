<h1 align="center">📡 Web Services | Projet 1</h1>
<h6 align="center">Projet réalisé par [Enzo Avagliano](https://github.com/EloxFire)</h6>

- 👨‍💻 Technologies du projet : **NodeJS / ExpressJS**

- 📫 Email de contact **enzo.avagliano@ynov.com**

<h3 align="left" style="margin-top:30px;margin-bottom: 20px;">⚙️ Configuration du projet</h3>

 - Clonez le projet
 ```bash
git clone https://gitlab.com/ynov-cours/m1-web-services/web-services-get-started.git
 ```



  - Installez les dépendances avec npm
 ```bash
npm i
 ```

   - Installer la base de donnée grace au script SQL fournis
 ```bash
 # INFO: Le script SQL est a la racine du repoertoire Git que vous venez de cloner

# Dans votre console MySQL
source C:\\path_to\the\sql_script.sql
 ```

   - Configurez le projet
 ```bash
- Dupliquez le fichier 'database.config.exemple.js', renommez le 'database.config.js'
- Saisissez vos identifiants MySQL
 ```

  - Lancez le projet
 ```bash
node ./bin/www/js
# ou alors
npm start
 ```


<div style="margin-top: 30px;margin-bottom: 50px;"></div>

