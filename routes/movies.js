var express = require('express');
var router = express.Router();
const db = require('../src/modules/databaseModule.js');

/* GET all movies. */
router.get('/', function (req, res, next) {
  db.query("SELECT * FROM movies", function (err, rows, fields) {
    if (err) {
      console.log(err);
      res.status(500).end();
    } else {
      res.status(200).send(rows);
    }
  });
});

router.get('/:id', function (req, res, next) {
  res.set({
    'Content-Type': 'application/json',
    'Accept': 'JSON'
  });

  db.query(`SELECT * FROM movies WHERE id=${req.params.id}`, function (err, rows, fields) {
    if (err) {
      console.log(err);
      res.status(500).end();
    } else {
      if (rows.length == 0) {
        res.status(404).send({ error: "NOT FOUND" });
        return;
      }
      res.status(200).send(rows);
    }
  });
});

router.post('/create', function (req, res, next) {
  res.set({
    'Content-Type': 'application/json',
    'Accept': 'JSON'
  });

  db.query(`INSERT INTO movies(name, description, release_date, mark) VALUES ('${req.body.name}', '${req.body.description}', '${req.body.release_date}', ${req.body.mark})`, function (err, rows, fields) {
    if (err) {
      console.log(err);
      res.status(500).end();
    } else {
      res.status(201).send(rows);
    }
  });
});


router.delete('/delete/:id', function (req, res, next) {
  db.query(`DELETE * FROM movies WHERE id = ${req.params.id}`, function (err, rows, fields) {
    if (err) {
      console.log(err);
      res.status(500).end();
    } else {
      res.status(200).end();
    }
  });
});

router.patch('/update/:id', function (req, res, next) {
  db.query(`UPDATE movies SET name = '${req.body.name}', description = '${req.body.description}', release_date = '${req.body.date}', mark = ${req.body.mark} WHERE id = ${req.params.id}`, function (err, rows, fields) {
    if (err) {
      console.log(err);
      res.status(500).end();
    } else {
      res.status(200).send(rows);
    }
  });
});




module.exports = router;
