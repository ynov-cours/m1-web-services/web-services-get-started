DROP DATABASE IF EXISTS avagliano_web_services_projet_un;
CREATE DATABASE IF NOT EXISTS avagliano_web_services_projet_un;
USE avagliano_web_services_projet_un;

CREATE TABLE `movies` (
	`id` INT NOT NULL AUTO_INCREMENT,
	`name` VARCHAR(128),
	`description` TEXT(2048),
	`release_date` DATE,
	`mark` INT,
	PRIMARY KEY (`id`)
) ENGINE=InnoDB;

INSERT INTO movies(name, description, release_date, mark) VALUES('Interstellar', "Alors que la Terre se meurt, une équipe d'astronautes franchit un trou de ver apparu près de Saturne et conduisant à une autre galaxie, afin d'explorer un nouveau système stellaire et dans l'espoir de trouver une planète habitable et y établir une colonie spatiale pour sauver l'humanité.", '2014-11-05', 5);