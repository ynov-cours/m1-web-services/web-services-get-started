module.exports = {
  MYSQL: {
    HOST: "your_host",
    USER: "your_mysql_user",
    PASS: 'your_mysql_user_pass',
    DATABASE: 'avagliano_web_services_projet_un', // Si vous avez utilisé le script fournis ^^
    PORT: 3306
  }
}